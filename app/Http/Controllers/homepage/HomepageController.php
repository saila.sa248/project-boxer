<?php

namespace App\Http\Controllers\homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\admin\product;
use App\customer;
use Session;
use Hash;
use App\admin\cart;
use App\admin\checkout;
use App\admin\checkout_item;
use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;
use App\contact_us;


class HomepageController extends Controller
{
    public function shop_grid() {
        $product = product::where('prod_status', 1)->get();
        $product_current = $product->count();
        $product_total = product::where('prod_status',1)->count();
        return view('homepage.shop-grid', [
            'product' => $product, 'product_total' => $product_total, 'product_current' => $product_current
        ]);
    }

    public function header() {
        return view('homepage.header');
    }

    public function addToCart($id, Request $request)
    {
        if(session::get('cust_login')) {
            $cust = session()->get('cust_id');
            $cart_update = cart::where('cust_id', $cust)->first();
            $product = product::findOrFail($id);
            $cart = session()->get('cart', []);
            $prod_after_discount = 0;
            if ($product->prod_discount != 0) {
                $prod_after_discount = $product->prod_price_reguler - $product->prod_discount;
            }
            if(isset($cart[$id])) {
                $qty = $cart[$id]['quantity']++;
                // dd($cart[$id]);
                $cart_update->cart_qty = $cart[$id]['quantity'];
                $cart_update->rec_editor = 'By System';
                $cart_update->updated_at = now();
                // dd($cart_update); 
                // dd($cart_update);
                $cart_update->save();
            } else {
                $cart[$id] = [
                    "prod_id" => $product->prod_id,
                    "name" => $product->prod_name,
                    "quantity" => 1,
                    "price" => $product->prod_price_reguler,
                    "price_discount" => $product->prod_discount,
                    "prod_after_discount" => $prod_after_discount,
                    "image" => $product->prod_image,
                    "desc" => $product->prod_desc,
                ];
                $cart_store = new cart;
                $cart_store->cust_id = $cust;
                $cart_store->prod_id = $product->prod_id;
                $cart_store->cart_status = 1;
                $cart_store->cart_qty = $cart[$id]['quantity'];
                $cart_store->rec_creator = 'By System';
                $cart_store->rec_editor = 'By System';
                $cart_store->created_at = now();
                $cart_store->updated_at = now();
                $cart_store->save();
            }
        } else {
            return redirect('/loginCust');
        }
        
          
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function addCartTable() {
        $cart = session()->get('cart');

        return view('homepage.cart', compact('cart'));
    }

    public function loginCust(){

        return view('homepage.login');
    }

    public function signinCust(Request $request) {
        $this->validate($request, [
            'cust_email' => 'required',
            'cust_password' => 'required'
        ], [
            'cust_email.required' => 'Email harus diisi!',
            'cust_password.required' => 'Password harus diisi!'
        ]);

        $email = $request->cust_email;
        $password = $request->cust_password;
        $cust = customer::where('cust_email', $email)->where('cust_status', 1)->first();

        if($cust){
            if(Hash::check($password, $cust->cust_password)){
                Session::put('cust_id', $cust->cust_id);
                Session::put('cust_name', $cust->cust_name);
                Session::put('cust_email', $cust->cust_email);
                Session::put('cust_alamat', $cust->cust_alamat);
                Session::put('cust_provincy', $cust->cust_provincy);
                Session::put('cust_kota', $cust->cust_kota);
                Session::put('cust_kecamatan', $cust->cust_kecamatan);
                Session::put('cust_kode_pos', $cust->cust_kode_pos);
                Session::put('cust_no_telp', $cust->cust_no_telp);
                $testSession = Session::put('cust_login', TRUE);
                
                return redirect('/shop-grid')->with('success', 'Login Berhasil', 'Selamat Datang ' );
            } else {
                return redirect()->back()->with('password', 'Password Salah!!');;
            }
        } else { 
                return redirect()->back()->with('tidak-aktf', 'Email tidak terdaftar atau belum di approve Admin Boxer!');;
        }
    }

    public function register() {

        return view('homepage.register');
    }

    public function registerCust(Request $request) {

        $this->validate($request, [
            'cust_name' => 'required|max:100',
            'cust_password' => 'required|max:50',
            'cust_email' => 'required|max:100|unique:customer',
            'cust_alamat' => 'required',
            'cust_provincy' => 'required|max:100',
            'cust_kota' => 'required|max:100',
            'cust_kecamatan' => 'required|max:100',
            'cust_kode_pos' => 'required|max:100',
            'cust_no_telp' => 'required|max:100'
        ], [
            'cust_name.required' => 'Nama harus diisi',
            'cust_name.max' => 'Nama maksimal 100 karakter',
            'cust_password.required' => 'Password harus diisi',
            'cust_password.max' => 'Password maksimal 50 karakter',
            'cust_email.required' => 'Email  harus diisi',
            'cust_email:unique' => 'Email sudah terdaftar',
            'cust_email.max' => 'Email maksimal 100 karakter',
            'cust_alamat.required' => 'Alamat harus diisi',
            'cust_provincy.required' => 'Provincy harus diisi',
            'cust_provincy.max' => 'Provincy maksimal 100 karakter',
            'cust_kota.required' => 'Kota harus diisi',
            'cust_kota.max' => 'Kota maksimal 100 karakter',
            'cust_kecamatan.required' => 'Kecamatan harus diisi',
            'cust_kecamatan.max' => 'Kecamatan maksimal 100 karakter',
            'cust_kode_pos.required' => 'Kode Pos harus diisi',
            'cust_kode_pos.max' => 'Kode Pos maksimal 100 karakter',
            'cust_no_telp.required' => 'No Telepon harus diisi',
            'cust_no_telp.max' => 'No Telepon maksimal 100 karakter'
        ]);

        $password = $request->cust_password;
        $password = bcrypt($password);

        $createCustomer = new customer;
        $createCustomer->cust_name = ucwords($request->cust_name);
        $createCustomer->cust_password = $password;
        $createCustomer->cust_email = $request->cust_email;
        $createCustomer->cust_alamat = ucwords($request->cust_alamat);
        $createCustomer->cust_provincy = ucwords($request->cust_provincy);
        $createCustomer->cust_kota = ucwords($request->cust_kota);
        $createCustomer->cust_kecamatan = ucwords($request->cust_kecamatan);
        $createCustomer->cust_kode_pos = $request->cust_kode_pos;
        $createCustomer->cust_no_telp = $request->cust_no_telp;
        $createCustomer->cust_status = 2;
        $createCustomer->rec_creator = 'Via Website';
        $createCustomer->rec_editor = 'Via Website';
        $createCustomer->created_at = now();
        $createCustomer->updated_at = now();
        $createCustomer->save();

        return redirect('/loginCust')->with('register-success', 'Registrasi berhasil silahkan hubungi admin kami untuk Aktifkan akun');
    }

    public function logout() {
        Session()->get('cust_login');
        Session()->forget('cust_login');
        Session()->save();

        return redirect('/shop-grid');
    }

    public function checkout()
    {
        if(session::get('cust_login')) {
            $cust = session()->get('cust_id');
            $cart_id = cart::where('cust_id', $cust)->where('cart_status', 1)->get();
            $cart = session()->get('cart');
    
            return view ('homepage.checkout', compact('cart', 'cart_id'));
        } else {
            return redirect('/loginCust');
        }
        
    }

    public function checkout_store(Request $request)
    {
        if(session::get('cust_login')) {

            // $this->validate($request, [
            //     'cust_name' => 'required|max:100',
            //     'cust_email' => 'required|max:100|unique:customer',
            //     'cust_alamat' => 'required',
            //     'cust_provincy' => 'required|max:100',
            //     'cust_kota' => 'required|max:100',
            //     'cust_kecamatan' => 'required|max:100',
            //     'cust_kode_pos' => 'required|max:100',
            //     'cust_no_telp' => 'required|max:100',
            //     'bukti_bayar' => 'required'
            // ], [
            //     'cust_name.required' => 'Nama harus diisi',
            //     'cust_name.max' => 'Nama maksimal 100 karakter',
            //     'cust_email.required' => 'Email  harus diisi',
            //     'cust_email:unique' => 'Email sudah terdaftar',
            //     'cust_email.max' => 'Email maksimal 100 karakter',
            //     'cust_alamat.required' => 'Alamat harus diisi',
            //     'cust_provincy.required' => 'Provincy harus diisi',
            //     'cust_provincy.max' => 'Provincy maksimal 100 karakter',
            //     'cust_kota.required' => 'Kota harus diisi',
            //     'cust_kota.max' => 'Kota maksimal 100 karakter',
            //     'cust_kecamatan.required' => 'Kecamatan harus diisi',
            //     'cust_kecamatan.max' => 'Kecamatan maksimal 100 karakter',
            //     'cust_kode_pos.required' => 'Kode Pos harus diisi',
            //     'cust_kode_pos.max' => 'Kode Pos maksimal 100 karakter',
            //     'cust_no_telp.required' => 'No Telepon harus diisi',
            //     'cust_no_telp.max' => 'No Telepon maksimal 100 karakter',
            //     'bukti_bayar.required' => 'Bukti bayar harus ada'
            // ]);

            $cust_id = session()->get('cust_id');
            $cart = cart::where('cust_id', $cust_id)->get();

            $code = 'OR';
            $tanggal = date('ymd');
            $today = Carbon::today();
            $checkout = checkout::whereDate('created_at', $today)->count();
            $checkout = $checkout + 1;
            $fix = $code . $tanggal . $checkout;

            $createCheckout = new checkout;
            $createCheckout->co_code = $fix;
            $createCheckout->cust_id = $cust_id;
            $createCheckout->cust_name = $request->cust_name;
            $createCheckout->cust_email = $request->cust_email;
            $createCheckout->cust_alamat = $request->cust_alamat;
            $createCheckout->cust_provincy = $request->cust_provincy;
            $createCheckout->cust_kota = $request->cust_kota;
            $createCheckout->cust_kecamatan = $request->cust_kecamatan;
            $createCheckout->cust_kode_pos = $request->cust_kode_pos;
            $createCheckout->cust_no_telp = $request->cust_no_telp;
            if($file = $request->hasFile('bukti_bayar')) {
                $file = $request->file('bukti_bayar');
                $nama_file = time() . "_" . $file->getClientOriginalName();
                $file->move(\base_path() . "/public/images/bayar/", $nama_file);
                $createCheckout->bukti_bayar = $nama_file;
            } else {
                $createCheckout->bukti_bayar = 'Tidak ada bukti bayar';
            }
            $createCheckout->co_status = 1;
            $createCheckout->rec_creator = 'Via Website';
            $createCheckout->rec_editor = 'Via Website';
            $createCheckout->created_at = now();
            $createCheckout->updated_at = now();
            $saveCO = $createCheckout->save();

            if($saveCO) {
                foreach($cart as $co) {
                    $product =  $co::join('product', function($query) {
                                                $query->on('product.prod_id', 'cart.prod_id');
                                            })->where('product.prod_id', $co->prod_id)->first();
                    $prod_price_reguler = $product->prod_price_reguler;
                    $prod_price_discount = $product->prod_discount;
                    $prod_price_after_discount = $prod_price_reguler - $prod_price_discount;
                    $prod_qty = $co->cart_qty;

                    $createCheckoutItem = new checkout_item;
                    $createCheckoutItem->co_id = $createCheckout->co_id;
                    $createCheckoutItem->prod_id = $co->prod_id;
                    $createCheckoutItem->coi_prod_price_reguler = $prod_price_reguler;
                    $createCheckoutItem->coi_prod_discount = $prod_price_discount;
                    $createCheckoutItem->coi_prod_qty = $prod_qty;
                    $createCheckoutItem->coi_prod_after_discount = $prod_price_after_discount;
                    $createCheckoutItem->coi_fix_reguler = $prod_price_reguler * $prod_qty;
                    $createCheckoutItem->coi_fix_discount = $prod_price_discount * $prod_qty;
                    $createCheckoutItem->coi_fix_sales = $prod_price_after_discount * $prod_qty;
                    $createCheckoutItem->coi_status = 1;
                    $createCheckoutItem->rec_creator = 'Via Website';
                    $createCheckoutItem->rec_editor = 'Via Website';
                    $createCheckoutItem->created_at = now();
                    $createCheckoutItem->updated_at = now();
                    $saveCoItem = $createCheckoutItem->save();
                    if($saveCoItem) {
                        Session::get('cart', []);
                        Session::forget('cart', []);
                        Session::save('cart');
                        $success = $co->where('prod_id', $product->prod_id)->where('cust_id', $cust_id)->delete();
                    }
                }
            }
            if($success) {
                return redirect('/shop-grid')->with('success-checkout', 'Checkout berhasil, Harap tunggu untuk di Proses');
            }
        } else {
            return redirect('/loginCust');
        }
    }

    public function contact() {
        return view('homepage.contact');
    }

    public function contactStore(Request $request) {

        $contact_us = new contact_us;
        $contact_us->cu_name = $request->cu_name;
        $contact_us->cu_subjek = $request->cu_subjek;
        $contact_us->cu_email = $request->cu_email;
        $contact_us->cu_phone = $request->cu_phone;
        $contact_us->cu_message = $request->cu_message;
        $contact_us->cu_status = 1;
        $contact_us->rec_creator = 'Customer Web';
        $contact_us->rec_editor = 'Customer Web';
        $contact_us->created_at = now();
        $contact_us->updated_at = now();
        $contact_us->save();

        Session::put('contact-us-success', TRUE);
        return redirect('/contact');
    }

    public function searchProduct(Request $request) {
        $search = $request->search;
        // dd($search);
        $product = product::where('prod_name', 'like','%'.$search.'%')->where('prod_status', 1)->get();
        $product_current = $product->count();
        $product_total = product::where('prod_status',1)->count();
        return view('homepage.shop-grid', [
            'product' => $product, 'product_total' => $product_total, 'product_current' => $product_current
        ]);
    }
}
