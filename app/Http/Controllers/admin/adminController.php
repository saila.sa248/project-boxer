<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use Hash;
use Alert;
use Illuminate\Support\Facades\Session;
use Auth;
use App\contact_us;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = admin::where('admin_status', 1)->get();
        return view('admin.login_admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ], [
            'username.required' => 'Username harus diisi!',
            'password.required' => 'Password harus diisi!'
        ]);

        $username = $request->username;
        $password = $request->password;
        $admin = admin::where('admin_username', $username)->where('admin_status', 1)->first();
        $contact_us = contact_us::where('cu_status', 1)->first();
        // dd($contact_us);

        if($admin){
            if(Hash::check($password, $admin->admin_password)){
                Session::put('name', $admin->admin_name);
                Session::put('username', $admin->admin_username);
                Session::put('login', TRUE);
                Session::put('cu_name', $contact_us->cu_name);

                return redirect()->route('sales')->with('success', 'Login Berhasil', 'Selamat Datang ' );
            } else {
                return redirect()->back()->with('error', 'Username / Password Salah');
            }
        } else { 
                return redirect()->back()->with('error', 'Username tidak terdaftar!');        
        }
    }

    public function logout() {
        Session::get('login');
        Session::forget('login');
        Session::save();

        return redirect('/admin')->with('success-logout', 'Berhasil logout !');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function contactUsAdmin() {
        $contact_us = contact_us::where('cu_status', 1)->limit(10);
        dd($contact_us);

        return view('admin.layouts.header', [
            'contact_us' => $contact_us
        ]);
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
