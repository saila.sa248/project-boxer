<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\customer;

class CustController extends Controller
{
    public function index(){
        if(session::get('login')) {
            $cust = customer::all();
            return view('admin.pelanggan.index', compact('cust'));
        } else {
            return redirect('admin');
        }
    }

    public function custDetail($id){
        if(session::get('login')) {
            $cust = customer::where('cust_id', $id)->get();
            return view('admin.pelanggan.detail', compact('cust'));
        } else {
            return redirect('admin');
        }
    }

    public function custEdit($id) {
        if(session::get('login')) {
            $cust = customer::where('cust_id', $id)->get();
            return view('admin.pelanggan.edit', [
                'cust' => $cust
            ]);
        } else {
            return redirect('admin');
        }
    }

    public function custUpdate(Request $request){
        if(session::get('login')) {
            // $cust_id = $request->cust_id;
            $updateCustomer = customer::where('cust_id', $request->cust_id)->first();
            // dd($updateCustomer);
            $status = $updateCustomer->cust_status = $request->cust_status;
            $updateCustomer->rec_editor = Session::get('name');
            $updateCustomer->updated_at = now();
            $aktif = 'Customer ' . $updateCustomer->cust_name . ' berhasil di Aktifkan';
            $nonaktif = 'Customer ' . $updateCustomer->cust_name . ' berhasil di Non-Aktifkan';

            // dd($status);
            if($status == 1) {
                $updateCustomer->save();
                return redirect('/cust')->with('success-aktif', $aktif);
            } else {
                $updateCustomer->save();
                return redirect('/cust')->with('success-inactive', $nonaktif);
            }
        } else{
            return redirect('admin');
        }
    }
}
