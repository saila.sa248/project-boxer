<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\admin\checkout_item;
use App\admin\checkout;
use Carbon\Carbon;
use DB;

class laporanController extends Controller
{
    public function index(){
        if(session::get('login')) {
            
            $laporan = checkout_item::select(DB::raw('convert(created_at, date) as day'), 
            // DB::raw('DAY(created_at) as daytwo'), 
            DB::raw('sum(coi_fix_reguler) as prod_price_reguler'), 
            DB::raw('sum(coi_fix_discount) as discount'), DB::raw('sum(coi_fix_sales) as total'))->groupBy('day')->get();
            
            // $laporan = checkout_item::selectRaw('DAY(created_at) as day')

            $my = date('F - Y');

            $checkout_item = checkout_item::where('coi_status', 1);
            $sales = $checkout_item->sum('coi_prod_price_reguler');
            $discount = $checkout_item->sum('coi_prod_discount');
            $total_sales = $checkout_item->sum('coi_fix_sales');

            return view('admin.laporan.index',[
                'sales' => $sales, 'discount' => $discount, 'total_sales' => $total_sales, 'laporan' => $laporan, 'my' => $my
            ]);
        } else {
            return redirect('admin');
        }
    }
}
