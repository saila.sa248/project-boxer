<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\admin\product;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session::get('login')) {
            $product = product::get();
            return view('admin.product.index', ['product' => $product]);
        } else {
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session::get('login')) {
            return view('admin.product.product_add');
        } else {
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(session::get('login')) {
            $this->validate($request, [
                'prod_name' => 'required|unique:product|max:255',
                'prod_price_reguler' => 'required',
                'prod_status' => 'required',
                'prod_code' => 'required|unique:product'
                // 'prod_image' => 'required|mimes:jpeg,jpg,png,gif,webp,svg,jfif'
            ], [
                'prod_name.required' => 'Nama Product harus diisi',
                'prod_name.unique' => 'Nama Product sudah ada',
                'prod_name.max' => 'Nama Product maksimal 255 karakter',
                'prod_price_reguler.required' => 'Product Price Reguler harus diisi',
                'prod_status.required' => 'Product Status harus diisi',
                'prod_code.unique' => 'Product Code sudah ada',
                'prod_code.required' => 'Product Code harus diisi',
                'prod_image.required' => 'Product Image harus diisi'
                // 'prod_image.mimes' => 'Product Image Extension harus jpg, jpeg, png, gif, webp, svg, jfif'
            ]);

            $createProduct = new product;
            $createProduct->prod_code = $request->prod_code;
            $createProduct->prod_name = $request->prod_name;
            $createProduct->prod_desc = $request->prod_desc;
            if($file = $request->hasFile('file')) {
                $file = $request->file('file');
                $nama_file = time() . "_" . $file->getClientOriginalName();
                $file->move(\base_path() . "/public/images/product/", $nama_file);
                $createProduct->prod_image = $nama_file;
            } else {
                $createProduct->prod_image = 'Tidak ada images';
            }
            $createProduct->prod_price_reguler = $request->prod_price_reguler ? : 0;
            $createProduct->prod_discount = $request->prod_discount ? : 0;
            $createProduct->prod_after_discount = $request->prod_price_reguler - $request->prod_discount ? : 0;
            // $createProduct->prod_stock;
            $createProduct->prod_cat = $request->prod_cat ? : 1;
            $createProduct->prod_status = $request->prod_status ? : 1;
            $createProduct->created_at = now();
            $createProduct->updated_at = now();
            $createProduct->rec_creator = Session::get('name');
            $createProduct->rec_editor = Session::get('name');
            $createProduct->save();
            
            return redirect('/product')->with('success', 'Product berhasil dibuat!');
        } else {
            return redirect('admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session::get('login')) {

        $product = product::where('prod_id', $id)->first();

        return view('admin.product.product_detail',compact('product'));
        } else {
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session::get('login')) {

            $product = product::where('prod_id', $id)->first();

            return view('admin.product.product_edit', compact('product'));
        } else {
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(session::get('login')) {
            $prod_id = $request->prod_id;
            $this->validate($request, [
                'prod_name' => ['required', Rule::unique('product')->ignore($prod_id, 'prod_id')],
                'prod_price_reguler' => 'required',
                'prod_status' => 'required',
                'prod_code' => ['required', Rule::unique('product')->ignore($prod_id, 'prod_id')]
                // 'prod_image' => 'required|mimes:jpeg,jpg,png,gif,webp,svg,jfif'
            ], [
                'prod_name.required' => 'Nama Product harus diisi',
                'prod_name.unique' => 'Nama Product sudah ada',
                'prod_name.max' => 'Nama Product maksimal 255 karakter',
                'prod_price_reguler.required' => 'Product Price Reguler harus diisi',
                'prod_status.required' => 'Product Status harus diisi',
                'prod_code.unique' => 'Product Code sudah ada',
                'prod_code.required' => 'Product Code harus diisi',
                'prod_image.required' => 'Product Image harus diisi'
                // 'prod_image.mimes' => 'Product Image Extension harus jpg, jpeg, png, gif, webp, svg, jfif'
            ]);

            $updateProduct = product::where('prod_id', $prod_id)->first();
            $updateProduct->prod_name = $request->prod_name ? : $updateProduct->prod_name;
            $updateProduct->prod_code = $request->prod_code ? : $updateProduct->prod_code;
            $updateProduct->prod_desc = $request->prod_desc ? : $updateProduct->prod_desc;
            $updateProduct->prod_price_reguler = $request->prod_price_reguler ? : $updateProduct->prod_price_reguler;
            $updateProduct->prod_discount = $request->prod_discount ? : $updateProduct->prod_discount;
            $updateProduct->prod_after_discount = $request->prod_price_reguler - $request->prod_discount ? : $updateProduct->prod_after_discount;
            // $updateProduct->prod_stock;
            $updateProduct->prod_cat = $request->prod_cat ? : $updateProduct->prod_cat;
            $updateProduct->prod_status = $request->prod_status ? : $updateProduct->prod_status;
            
            if($file = $request->hasFile('file')) {
                $file = $request->file('file');
                $nama_file = time() . "_" . $file->getClientOriginalName();
                $file->move(\base_path() . "/public/images/product/", $nama_file);
                $updateProduct->prod_image = $nama_file ? : $updateProduct->prod_image;
            } else {
                $updateProduct->prod_image = $updateProduct->prod_image;
            }
            $updateProduct->updated_at = now();
            $updateProduct->rec_editor = Session::get('name');
            $updateProduct->save();
    
            return redirect('/product_detail/'.$request->prod_id);
        } else {
            return redirect('admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
