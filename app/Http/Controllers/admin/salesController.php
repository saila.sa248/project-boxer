<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\admin\checkout;
use App\admin\checkout_item;
use Carbon\Carbon;

class salesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session::get('login')) {
            $checkout = checkout::all();
            $status1 = $checkout->where('co_status', 1)->count();
            $status2 = $checkout->where('co_status', 2)->count();
            $status3 = $checkout->where('co_status', 3)->count();
            $status4 = $checkout->where('co_status')->count();
            // $code = 'OR';
            // // $tanggal = date('ymd');
            // $today = Carbon::today();
            // $checkout = checkout::whereDate('created_at', $today)->count();
            // $checkout = $checkout + 1;
            // $fix = $code . $tanggal . $checkout;
            // dd($fix);

            return view('admin.sales.index', [
                'checkout' => $checkout, 'status1' => $status1, 'status2' => $status2, 'status3' => $status3, 'status4' => $status4
            ]);
        } else {
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session::get('login')) {
            $checkout = checkout::where('co_id', $id)->get();
            // dd($checkout);
            $checkout_item = checkout_item::where('co_id',$id)->where('coi_status', 1)->get();

            return view('admin.sales.detail', [
                'checkout' => $checkout, 'checkout_item' => $checkout_item
            ]);
        } else {
           return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateCheckout = checkout::where('co_id', $request->co_id)->first();
        $updateCheckout->co_status = $request->co_status;
        $updateCheckout->rec_editor = Session::get('name');
        $updateCheckout->updated_at = now();
        $updateCheckout->save();

        return redirect('/sales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
