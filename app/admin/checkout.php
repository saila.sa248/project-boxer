<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class checkout extends Model
{
    // use AutoNumberTrait;

    protected $table = 'checkout';
    protected $primaryKey = 'co_id';

    public function checkoutItem(){
        return $this->hasMany('App\admin\checkout_item', 'co_id');
    }

    // public function getAutoNumberOptions()
    // {
    //     return [
    //         'code' => [
    //             'format' => function () {
    //                 return 'OR' . date('Y.d.m') . '?'; 
    //             },
    //             'length' => 3,
    //         ]
    //     ];
    // }
    
}
