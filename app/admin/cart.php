<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    protected $table = 'cart';
    protected $primaryKey = 'cart_id';

    public function customer(){
        return $this->hasMany('App\customer', 'cust_id');
    }

    public function product(){
        return $this->hasMany('App\admin\product', 'prod_id');
    }
}
