<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'prod_id';

    public function cart(){
        return $this->belongsTo('App\admin\cart');
    }
}
