<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class checkout_item extends Model
{
    protected $table = 'checkout_item';
    protected $primaryKey = 'coi_id';

    public function checkout() {
        return $this->belongsTo('App\admin\checkout');
    }
}
