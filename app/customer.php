<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $table = 'customer';
    protected $primaryKey = 'cust_id';

    public function cart(){
        return $this->belongsTo('App\admin\cart', 'cust_id');
    }
}
