<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// PAGE ADMIN
Route::get('/admin', 'admin\adminController@index')->name('admin');
Route::post('/signIn', 'admin\adminController@store')->name('signIn');
Route::get('/login', function () {
    return view('admin.login_admin');
});
Route::GET('/logoutAdmin', 'admin\adminController@logout')->name('admin-logout');
Route::get('/header', 'admin\adminController@contactUsAdmin');

// PAGE PRODUCT
Route::get('/product', 'admin\productController@index')->name('product');
Route::get('/add_prod', 'admin\productController@create')->name('product_add');
Route::post('store-prod', 'admin\productController@store')->name('prod-store');
Route::get('/product_detail/{id}', 'admin\productController@show')->name('product_detail');
Route::get('/product_edit/{id}', 'admin\productController@edit')->name('product_edit');
Route::post('/update-prod', 'admin\productController@update')->name('prod-update');
// Route::resource('/product', 'admin\productController');

// PAGE SALES
Route::get('/sales','admin\salesController@index')->name('sales');
Route::get('/sales-detail/{id}', 'admin\salesController@show')->name('sales-detail');
Route::post('/sales-update', 'admin\salesController@update')->name('sales-update');

// PAGE PELANGGAN
Route::get('/cust','admin\custController@index')->name('customer');
Route::get('/cust-detail/{id}','admin\custController@custDetail')->name('customer-detail');
Route::get('/cust-edit/{id}', 'admin\custController@custEdit')->name('customer-edit');
Route::post('/update-cust', 'admin\custController@custUpdate')->name('customer-update');

// PAGE LAPORAN
Route::get('/laporan','admin\laporanController@index')->name('laporan');

// HOMEPAGE
Route::get('/', function () {
    return view('homepage.layouts.master');
});
Route::get('/checkout', 'homepage\HomepageController@checkout')->name('checkout');
Route::post('/checkout-store', 'homepage\HomepageController@checkout_store')->name('checkout-store');

Route::get('register', 'homepage\HomepageController@register')->name('register');
Route::post('/registerCust', 'homepage\HomepageController@registerCust')->name('register-customer');
Route::get('/loginCust', 'homepage\HomepageController@loginCust')->name('login-customer');
Route::post('/signinCust', 'homepage\HomepageController@signinCust')->name('signin-customer');
Route::get('/logout', 'homepage\HomepageController@logout')->name('logout-customer');

Route::get('/shop-grid','homepage\HomepageController@shop_grid');
Route::get('/headerWeb', 'homepage\HomepageController@header')->name('header');
Route::get('/cart', 'homepage\HomepageController@addCartTable')->name('cart-table');

Route::get('/searchProduct', 'homepage\HomepageController@searchProduct')->name('search-product');
Route::get('/paginateProduct', 'homepage\HomepageController@paginateProduct')->name('paginate-product');

Route::get('add-to-cart/{id}', 'homepage\HomepageController@addToCart')->name('add.to.cart');
// Route::post('/cart-add', 'homepage\HomepageController@')

Route::get('/contact', 'homepage\HomepageController@contact')->name('contact');
Route::post('/contact-store', 'homepage\HomepageController@contactStore')->name('contact-store');

Route::get('/register', function(){
    return view('homepage.register');
});
