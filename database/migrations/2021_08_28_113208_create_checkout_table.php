<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout', function (Blueprint $table) {
            $table->increments('co_id');
            $table->string('co_code')->nullable();
            $table->integer('cust_id');
            $table->string('cust_name');
            $table->string('cust_email')->nullable();
            $table->text('cust_alamat')->nullable();
            $table->string('cust_provincy')->nullable();
            $table->string('cust_kota')->nullable();
            $table->string('cust_kecamatan')->nullable();
            $table->string('cust_kode_pos')->nullable();
            $table->string('cust_no_telp')->nullable();
            $table->string('bukti_bayar')->nullable();
            $table->enum('co_status', [1,2,3,4])->comment('1:open, 2:confirmed, 3: process, 4:closed');
            $table->integer('kurir_id')->nullable();
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout');
    }
}
