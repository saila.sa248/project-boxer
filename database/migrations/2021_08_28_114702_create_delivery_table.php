<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery', function (Blueprint $table) {
            $table->increments('do_id');
            $table->integer('co_id');
            $table->enum('do_status', [1,2])->comment('1: open (open,confirmed,proses), 2: proses (delivery), 3:closed (delivered)');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery');
    }
}
