<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('cust_id');
            $table->string('cust_name');
            $table->string('cust_password');
            $table->string('cust_email')->unique();;
            $table->text('cust_alamat')->nullable();
            $table->string('cust_provincy')->nullable();
            $table->string('cust_kota')->nullable();
            $table->string('cust_kecamatan')->nullable();
            $table->string('cust_kode_pos')->nullable();
            $table->string('cust_no_telp')->nullable();
            $table->enum('cust_status', [1,2])->comment('1 active, 2 inactive');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
