<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('admin_id');
            $table->string('role')->nullable();
            $table->string('admin_name');
            $table->string('admin_username')->unique();
            $table->string('admin_password');
            $table->string('admin_email')->nullable();
            $table->enum('admin_status', [1,2])->comment('1 active, 2 inactive');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
