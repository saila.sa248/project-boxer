<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_us', function (Blueprint $table) {
            $table->increments('cu_id');
            $table->string('cu_name')->nullable();
            $table->string('cu_subjek')->nullable();
            $table->string('cu_email')->nullable();
            $table->string('cu_phone')->nullable();
            $table->text('cu_message')->nullable();
            $table->enum('cu_status', [1,2])->comment('1 active, 2 inactive');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_us');
    }
}
