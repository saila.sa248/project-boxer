<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_item', function (Blueprint $table) {
            $table->increments('coi_id');
            $table->integer('co_id');
            $table->integer('prod_id');
            $table->integer('coi_prod_price_reguler')->nullable();
            $table->integer('coi_prod_discount')->nullable();
            $table->integer('coi_prod_qty')->nullable();
            $table->integer('coi_prod_after_discount')->nullable();
            $table->integer('coi_fix_sales')->nullable();
            $table->integer('coi_fix_reguler')->nullable();
            $table->integer('coi_fix_discount')->nullable();
            $table->enum('coi_status', [1,2])->comment('1 active, 2 inactive');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_item');
    }
}
