<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('prod_id');
            $table->string('prod_name');
            $table->string('prod_code')->unique();
            $table->text('prod_image');
            $table->text('prod_desc')->nullable();
            $table->integer('prod_price_reguler')->nullable();
            $table->integer('prod_discount')->nullable();
            $table->integer('prod_after_discount')->nullable();
            $table->integer('prod_stock')->nullable();
            $table->string('prod_cat')->nullable();
            $table->enum('prod_status', [1,2])->comment('1 active, 2 inactive');
            $table->string('rec_creator')->nullable();
            $table->string('rec_editor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
