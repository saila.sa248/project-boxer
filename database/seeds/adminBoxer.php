<?php

use Illuminate\Database\Seeder;

class adminBoxer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //admin Boxer
        DB::table('admin')->insert([
            'role' => 'Admin',
            'admin_name' => 'Admin Boxer',
            'admin_username' => 'admin',
            'admin_password' => bcrypt('adminboxer1231'),
            'admin_email' => '',
            'admin_status' => 1,
            'rec_creator' => 'systemSeed',
            'rec_editor' => 'systemSeed',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
