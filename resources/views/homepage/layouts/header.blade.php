<style>

@media (min-width: 576px) {
  .modal-dialog-login {
    max-width: 400px;
  }
  .modal-dialog-login .modal-content-login {
    padding: 1rem;
  }
}
.modal-header-login .close {
  margin-top: -1.5rem;
}

.btn-round {
  border-radius: 3rem;
}

.delimiter {
  padding: 1rem;
}

.social-buttons .btn {
  margin: 0 0.5rem 1rem;
}

.signup-section {
  padding: 0.3rem 0rem;
}
</style>
@csrf
<!-- Topbar -->
		<!-- End Topbar -->
		<div class="middle-inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-12">
						<!-- Logo -->
						<div class="logo">
							<a href="/"><img width="50%" src="images/logowee.png" alt="logo"></a>
						</div>
						<!--/ End Logo -->
					</div>
					<div class="col-lg-8 col-md-7 col-12">
						<div class="search-bar-top">
							<div class="search-bar">
								<form method="GET" action="/searchProduct">
									<input name="search" placeholder="Search Products Here....." type="search" autocomplete="off" value="{{old('search')}}">
									<button class="btnn" type="submit"><i class="ti-search"></i></button>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-12">
						<div class="right-bar">
							<!-- Search Form -->
							<div class="sinlge-bar shopping">
								<a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
								<div class="shopping-item" width="25%">
									@if(session('cust_login') == false)
									<div class="dropdown-cart-header" height="50%">
										<span>Silahkan login
										</span>
										<a href="{{route('login-customer')}}">Login  &nbsp;&nbsp;&nbsp;</a><br>
										<span>Silahkan Register
										</span>
										<a href="/register">Register &nbsp;&nbsp;&nbsp;</a><br>
									</div>
									{{-- <div class="dropdown-cart-header">
									</div> --}}
									@else
									<div class="dropdown-cart-header">
										<span>Hallo {{Session::get('cust_name')}}!</span>
										<a href="{{route('logout-customer')}}">Logout  &nbsp;&nbsp;&nbsp;</a><br>
									</div>
									@endif
								</div>
							</div>
							<div class="sinlge-bar shopping">
								@if(session('cart') == null)
								<form action="#">
								@else
								<form method="get" action="{{ route('cart-table') }}">
								@endif
								<a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count">{{ count((array) session('cart')) }}</span></a>
								<!-- Shopping Item -->
								<div class="shopping-item">
									<div class="dropdown-cart-header">
										<span>{{ count((array) session('cart')) }} Items</span>
										@if(session('cart') == null)
										<a href="#">View Cart</a>
										@else
										<a href="/cart">View Cart</a>
										@endif
									</div>
									@php $total = 0 @endphp
									@foreach((array) session('cart') as $id => $details)
										@php $total += $details['prod_after_discount'] * $details['quantity'] @endphp

									@if(session('cart'))
										<ul class="shopping-list">
											<li>
												<a href="#" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
												<a class="cart-img" href="#"><img src="{{asset('images/product/'.$details['image'])}}" alt="#"></a>
												<h4><a href="#">{{ $details['name'] }}</a></h4>
												<p class="quantity">{{ $details['quantity'] }}x - <span class="amount">{{ number_format($details['prod_after_discount'],0,'','.') }}</span></p>
											</li>
										</ul>
									@endif
									@endforeach
									
									<div class="bottom">
										<div class="total">
											<span>Total</span>
											<span class="total-amount">RP {{number_format($total,0,'','.')}}</span>
										</div>
										{{-- <div class="col-12"> --}}
											<input type="submit" class="btn col-12 animate" value="Checkout">
										{{-- </div> --}}
									</div>
								</div>
								<!--/ End Shopping Item -->
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Inner -->
		<div class="header-inner">
			<div class="container">
				<div class="cat-nav-head">
					<div class="row">
						<div class="col-md-3 col-lg-9 col-12">
							<div class="menu-area">
								<!-- Main Menu -->
								<nav class="navbar navbar-expand-lg">
									<div class="navbar-collapse">	
										<div class="nav-inner">	
											<ul class="nav main-menu menu navbar-nav" id="myDIV">
												<li><a href="/">Home</a></li>
												<li><a href="/shop-grid">Product</a></li>
													<!-- <li><a href="#">Shop<i class="ti-angle-down"></i><span class="new">New</span></a>
														<ul class="dropdown">
															<li><a href="/shop-grid">Shop Grid</a></li>
															<li><a href="cart.html">Cart</a></li>
															<li><a href="checkout.html">Checkout</a></li>
														</ul>
													</li> -->
												<li class=""><a href="/contact">Contact Us</a></li>
											</ul>
										</div>
									</div>
								</nav>
								<!--/ End Main Menu -->	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Header Inner -->
		<script>
		// Add active class to the current button (highlight it)
		var header = document.getElementById("myDIV");
		var btns = header.getElementsByClassName("btn");
		for (var i = 0; i < btns.length; i++) {
			btns[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");
				if (current.length > 0) { 
					current[0].className = current[0].className.replace(" active", "");
				}
				this.className += " active";
			});
		}
		</script>

<!-- Login Modal -->
{{-- @if(session('cust_login') == false)
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width:400px; padding:1rem" role="document">
		<div class="modal-content" style="padding:1rem">
			<div class="modal-header border-bottom-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="ti-close" aria-hidden="true"></span>
				</button>
			</div>
			<br>
			<form method="POST" action="{{ route('login-customer') }}">
			@csrf
			<div class="modal-body">
				<div class="text-center">
					<h4>Login Page</h4>
				</div>
				<br>
				<div class="d-flex flex-column text-center">
					<form>
					<div class="form-group">
						<input type="email" class="form-control" id="email1" name="cust_email" placeholder="Your email address..." required="required">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password1" name="cust_password" placeholder="Your password..." required="required">
					</div>
					<button type="submit" class="btn btn-info btn-block btn-round">Login</button>
					</form>
			
					<div class="text-center text-muted delimiter">or use a social network</div>
					<div class="d-flex justify-content-center social-buttons">
						<button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Twitter">
							<i class="fa fa-twitter"></i>
						</button>
						<button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Facebook">
							<i class="fa fa-facebook"></i>
						</button>
						<button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Linkedin">
							<i class="fa fa-linkedin"></i>
						</button>
					</div>
				</div>
			</div>
			</form>
			<div class="modal-footer d-flex justify-content-center">
				<div class="signup-section">Not a member yet? <a data-toggle="modal" data-target="#registerModal" title="Register" href="#" class="text-info"> Sign Up</a>.</div>
			</div>
		</div>
	</div>
</div>
@else 
	<p>ANDA SUDAH LOGIN</p>
@endif --}}
<!-- End Login Modal -->

<!-- Register Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width:400px; padding:1rem" role="document">
		<div class="modal-content" style="padding:1rem">
			<div class="modal-header border-bottom-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="ti-close" aria-hidden="true"></span>
				</button>
			</div>
			<br>
			<div class="modal-body">
				<div class="text-center">
					<h4>Create Account</h4>
				</div>
				<br>
				<div class="d-flex flex-column text-center">
					<form>
					<div class="form-group">
						<input type="text" class="form-control" id="nama1" placeholder="Nama">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" id="email1"placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password1" placeholder="Password">
					</div>
					<button type="button" class="btn btn-info btn-block btn-round">Register</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Register Modal -->
