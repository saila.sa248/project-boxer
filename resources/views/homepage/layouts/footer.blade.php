<!-- Footer Top -->
<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
								<a href="index.html"><img width="50%" src="images/logowee.png" alt="#"></a>
							</div>
							<p class="call">Contact Person<span><a href="tel:123456789">+62 877-4652-2234</a></span></p>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-5 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Information</h4>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Faq</a></li>
								<li><a href="#">Terms & Conditions</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Help</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer social">
							<h4>WeeStore Office</h4>
							<!-- Single Widget -->
							<div class="contact">
								<ul>
									<li>Jl. Margacinta, Mergasari</li>
									<li>Kec. Buah Batu</li>
									<li>Kota Bandung</li>
									<li>+62 877-4652-2234</li>
								</ul>
							</div>
							<!-- End Single Widget -->
							<ul>
								<li><a href="#"><i class="ti-facebook"></i></a></li>
								<li><a href="#"><i class="ti-twitter"></i></a></li>
								<li><a href="#"><i class="ti-flickr"></i></a></li>
								<li><a href="#"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="left">
								<p>Copyright © 2021 <a href="#" target="_blank">WeeStore</a>  -  All Rights Reserved.</p>
							</div>
						</div>
						<!-- <div class="col-lg-6 col-12">
							<div class="right">
								<img src="images/payments.png" alt="#">
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>