<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title>WeeStore</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{asset('/images/fav-icon.png')}}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.min.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="css/themify-icons.css">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="css/jquery-ui.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="css/flex-slider.min.css">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="css/owl-carousel.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="css/slicknav.min.css">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

	
	
</head>
<body class="js">
	
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
		
		<!-- Header -->
		<header class="header shop">
            @include('homepage.layouts.header')
		</header>
		<!--/ End Header -->
		
		<!-- Product Style -->
		<section class="product-area shop-sidebar shop section">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-12">
						<div class="shop-sidebar">
								<!-- Single Widget -->
								<div class="single-widget category">
									<h3 class="title">Categories</h3>
									<ul class="categor-list">
										<li><a href="/shop-grid">Boxer</a></li>
									</ul>
								</div>
								<!--/ End Single Widget -->
						</div>
					</div>
					@if(Session::has('success-checkout'))
						<script>
							var alert = {{Session::get('success-checkout')}};
							alert(alert);
							console.log('test');
						</script>
						{{Session::forget('success-checkout')}}
						{{Session::save('success-checkout')}}
					@endif
					<div class="col-lg-9 col-md-8 col-12">
						<div class="row">
							<div class="col-12">
								<form method="GET" url="/paginate">
								<div class="shop-top">
									<div class="shop-shorter">
										<div class="single-shorter">
											<label>Show :</label>
											<select id="show_table" onchange="showFunction()" name="qty_prod" disabled>
												<option selected="selected" value="{{$product_current}}">{{$product_current}}</option>
											</select>

											<script>
												var x = document.getElementById("mySelect").value;
												
											</script>

										</div>
										<div class="single-shorter">
											<label>From :</label>
											<select disabled>
												<option selected="selected" value="{{$product_total}}">{{$product_total}}</option>
											</select>
										</div>
									</div>
								</div>
								{{-- <input type="submit" class="btn col-7" value="Filter"> --}}
							</form>
							</div>
						</div>
						<div class="row" id="show_prod">
							@foreach($product as $prod)
							<div class="col-lg-4 col-md-6 col-12">
								<div class="single-product">
									<div class="product-img">
										<a href="#" title="Quick View" data-target="#exampleModal" data-toggle="modal">
											<img class="default-img" src="{{ asset('/images/product/'.$prod->prod_image) }}" alt="#">
											<img class="hover-img" src="{{ asset('/images/product/'.$prod->prod_image) }}" alt="#">
										</a>
										<div class="button-head">
											<div class="product-action">
												<a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
												{{-- <a title="Wishlist" href="#"><i class=" ti-heart "></i><span>Add to Wishlist</span></a> --}}
											</div>
											<div class="product-action-2">
												<a title="Add to cart" href="{{ route('add.to.cart', $prod->prod_id) }}">Add to cart</a>
											</div>
										</div>
									</div>
									<div class="product-content">
										<h3><a href="#" title="Quick View" data-target="#exampleModal" data-toggle="modal">{{ $prod->prod_name }}</a></h3>
										<div class="product-price">
											<span><del><small>@currency($prod->prod_price_reguler)</small></del></span>
											@php $disc = $prod->prod_price_reguler - $prod->prod_discount @endphp
											<br>
											<span style="color:#ee4d2d;">@currency($disc)</span>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Product Style 1  -->	
		
		<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
						</div>
						<div class="modal-body">
							<div class="row no-gutters">
								<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
									<!-- Product Slider -->
										<div class="product-gallery">
											<div class="quickview-slider-active">
												<div class="single-slider">
													<img src="{{ asset('images/product/1630505713_cek.jpg') }}" alt="#">
												</div>
												<div class="single-slider">
													<img src="{{ asset('images/product/1630505713_cek.jpg') }}" alt="#">
												</div>
												<div class="single-slider">
													<img src="{{ asset('images/product/1630505713_cek.jpg') }}" alt="#">
												</div>
												<div class="single-slider">
													<img src="{{ asset('images/product/1630505713_cek.jpg') }}" alt="#">
												</div>
											</div>
										</div>
									<!-- End Product slider -->
								</div>
								<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
									<div class="quickview-content">
										<h2>Celana kolor / Boxer motif</h2>
										<div class="quickview-ratting-review">
											<div class="quickview-ratting-wrap">
												<div class="quickview-ratting">
													<i class="yellow fa fa-star"></i>
													<i class="yellow fa fa-star"></i>
													<i class="yellow fa fa-star"></i>
													<i class="yellow fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<a href="#"> (100 customer review)</a>
											</div>
											<div class="quickview-stock">
												<span><i class="fa fa-check-circle-o"></i> in stock</span>
											</div>
										</div>
										<h3>Rp. 50.000</h3>
										<div class="quickview-peragraph">
											<p>Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)</p>
											<p><br>Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )</p>
											<p>Ukuran All Size</p>
											<p>✔ Untuk ukuran celana 29" - 36"
													✔ Panjang celana -/+ 40cm
													✔ Lingkar pinggang 110cm
													✔ Lingkar paha -/+ 70cm
													✔ Ada kantong di sebelah kanan
													✔ Jahitan Rapi sesuai standart garmen
											</p>
											</p><br>Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊</p>
											<br>
										</div>
										{{-- <div class="quantity"> --}}
											<!-- Input Order -->
											{{-- <div class="input-group">
												<div class="button minus">
													<button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
														<i class="ti-minus"></i>
													</button>
												</div>
												<input type="text" name="quant[1]" class="input-number"  data-min="1" data-max="1000" value="1">
												<div class="button plus">
													<button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[1]">
														<i class="ti-plus"></i>
													</button>
												</div>
											</div> --}}
											<!--/ End Input Order -->
										{{-- </div> --}}
										@foreach($product as $prod)
										<div class="add-to-cart">
											<a href="{{ route('add.to.cart', $prod->prod_id) }}" class="btn">Add to cart</a>
											{{-- <a href="#" class="btn min"><i class="ti-heart"></i></a> --}}
										</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal end -->
		
		<!-- Start Footer Area -->
		<footer class="footer">
			@include('homepage.layouts.footer')
		</footer>
		<!-- /End Footer Area -->
	
	
    <!-- Jquery -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<script src="js/colors.js"></script>
	<!-- Slicknav JS -->
	<script src="js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="js/magnific-popup.js"></script>
	<!-- Fancybox JS -->
	<script src="js/facnybox.min.js"></script>
	<!-- Waypoints JS -->
	<script src="js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="js/nicesellect.js"></script>
	<!-- Ytplayer JS -->
	<script src="js/ytplayer.min.js"></script>
	<!-- Flex Slider JS -->
	<script src="js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="js/easing.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>
</body>
</html>