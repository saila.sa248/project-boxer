<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title>WeeStore</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{asset('/images/fav-icon.png')}}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.min.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="css/themify-icons.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="css/flex-slider.min.css">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="css/owl-carousel.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="css/slicknav.min.css">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">

	
	
</head>
<body class="js">
	
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
		
		<!-- Header -->
		<header class="header shop">
			@include('homepage.layouts.header')
		</header>
		<!--/ End Header -->
				
		<!-- Start Checkout -->
		<section class="shop checkout section">
			<div class="container">
				<div class="row"> 
					<div class="col-lg-8 col-12">
						<div class="checkout-form">
							<h2>Make Your Checkout Here</h2>
							<p>Please register in order to checkout more quickly</p>
							<!-- Form -->
							<form class="form" method="post" action="{{ route('checkout-store') }}" enctype="multipart/form-data">
								@csrf
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Name<span>*</span></label>
											<input type="text" name="cust_name" value="{{ session()->get('cust_name') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Email Address<span>*</span></label>
											<input type="email" name="cust_email" value="{{ session()->get('cust_email') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Alamat Detail<span>*</span></label>
											<!-- <input type="number" name="number" placeholder="" required="required"> -->
											<textarea name="cust_alamat" value="{{ session()->get('cust_alamat') }}" required="required">{{ session()->get('cust_alamat') }}</textarea>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Provincy<span>*</span></label>
											<input type="text" name="cust_provincy" value="{{ session()->get('cust_provincy') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Kota<span>*</span></label>
											<input type="text" name="cust_kota" value="{{ session()->get('cust_kota') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Kecamatan<span>*</span></label>
											<input type="text" name="cust_kecamatan" value="{{ session()->get('cust_kecamatan') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>Kode Pos<span>*</span></label>
											<input type="number" name="cust_kode_pos" value="{{ session()->get('cust_kode_pos') }}" required="required">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="form-group">
											<label>No Hp<span>*</span></label>
											<input type="text" name="cust_no_telp" value="{{ session()->get('cust_no_telp') }}" required="required" minlength="10" maxlength="20">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										{{-- <div class="form-group"> --}}
										{{-- <div class="form-group" style="border: 5px"> --}}
											<label>Upload bukti bayar<span class="bukti-bayar">*</span></label>
											@if ($errors->has('bukti_bayar'))
												@foreach ($errors->get('bukti_bayar') as $error)
													<script>
													var error = <p class="error-message">{{ $error }}</p>
													alert(error);
													</script>
												@endforeach
											@endif
											{{-- <div class="custom-file"> --}}
												<input type="file" name="bukti_bayar" id="customFile" accept=".jpeg,.jpg,.png,.gif,.webp,.svg,.jfif" required="required">
												{{-- <label class="custom-file-label" for="customFile" selected>Image</label> --}}
												<script>
												  $(".custom-file-input").on("change", function() {
													var fileName = $(this).val().split("\\").pop();
													$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
												  });
												</script>
											  {{-- </div> --}}
										{{-- </div> --}}
									</div>
								</div>
							{{-- <input type="hidden" name="co_id" value="{{ session()->get('co_id') }}"> --}}
							<!--/ End Form -->
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="order-details">
							<!-- Order Widget -->
							<div class="single-widget">
								<h2>CART  TOTALS</h2>
								<div class="content">
									@php $total = 0 @endphp
									@foreach($cart as $c)
									@php $total += $c['prod_after_discount'] * $c['quantity'] @endphp
									@endforeach
									<ul>
										<li>Sub Total<span>Rp {{number_format($total,0,'','.')}}</span></li>
										<li>(+) Shipping<span>Free</span></li>
										<li class="last">Total<span>Rp {{number_format($total,0,'','.')}}</span></li>
									</ul>
								</div>
							</div>
							<!--/ End Order Widget -->
							<!-- Order Widget -->
							<div class="single-widget">
								<h2>Payments</h2>
								<div class="col-lg-12 ml-3">
									<label for="bt"><input type="checkbox" name="updates" id="bt" value="bank" class="checkbox checked" checked readonly>&nbsp;Bank Transfer</label>
								</div>
							</div>
							<!--/ End Order Widget -->
							<!-- Payment Method Widget -->
							<!-- <div class="single-widget payement">
								<div class="content">
									<img src="images/payment-method.png" alt="#">
								</div>
							</div> -->
							<!--/ End Payment Method Widget -->
							<!-- Button Widget -->
							<div class="single-widget get-button">
								<div class="content">
									<div class="button">
										<input type="submit" class="btn" value="CHECKOUT NOW">
									</div>
								</div>
							</div>
							<!--/ End Button Widget -->
						</div>
					</div>
					</form>
				</div>
			</div>
		</section>
		<!--/ End Checkout -->
		
		<!-- Start Shop Services Area  -->
		<section class="shop-services section home">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-rocket"></i>
							<h4>Free shiping</h4>
							<p>Orders Rp 0</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-reload"></i>
							<h4>Free Return</h4>
							<p>Within 30 days returns</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-lock"></i>
							<h4>Sucure Payment</h4>
							<p>100% secure payment</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-tag"></i>
							<h4>Best Peice</h4>
							<p>Guaranteed price</p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</section>
		<!-- End Shop Services -->
			
		<!-- Start Footer Area -->
		<footer class="footer">
			<!-- Footer Top -->
			@include('homepage.layouts.footer')
			<!-- End Footer Top -->
		</footer>
		<!-- /End Footer Area -->
 
	<!-- Jquery -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<script src="js/colors.js"></script>
	<!-- Slicknav JS -->
	<script src="js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="js/magnific-popup.js"></script>
	<!-- Fancybox JS -->
	<script src="js/facnybox.min.js"></script>
	<!-- Waypoints JS -->
	<script src="js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="js/nicesellect.js"></script>
	<!-- Ytplayer JS -->
	<script src="js/ytplayer.min.js"></script>
	<!-- Flex Slider JS -->
	<script src="js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="js/easing.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>
	<style>
		.bukti-bayar {
			color: #ff2c18;
			display: inline-block;
			position: absolute;
			top: 4px;
			font-size: 16px;
		}
		.error-message {
			color: #ff2c18;
			top: 4px;
			font-size: 16px;
		}
	</style>
</body>
</html>