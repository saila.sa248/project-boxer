@extends('admin.layouts.master')
@section('content')
<div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            <form method="POST" action="{{ route('prod-store') }}" enctype="multipart/form-data">
            @csrf
            <h3 class="card-title">Product Add</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputCode">Product Code</label>
                <input type="text" id="inputCode" class="form-control" name="prod_code" autocomplete="off" value="{{ old('prod_code') }}">
                @if ($errors->has('prod_code'))
                    @foreach ($errors->get('prod_code') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input type="text" id="inputName" class="form-control" name="prod_name" autocomplete="off" value="{{ old('prod_name') }}">
                @if ($errors->has('prod_name'))
                    @foreach ($errors->get('prod_name') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Description</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="prod_desc" value="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" placeholder="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" readonly></textarea>
              </div>
              <div class="form-group">
                <label for="inputStatus">Product Status</label>
                <select id="inputStatus" class="form-control custom-select" name="prod_status">
                  <option selected disabled>--- Select one ---</option>
                  <option value="1" selected>Active</option>
                  <option value="2">Inactive</option>
                </select>
                @if ($errors->has('prod_status'))
                    @foreach ($errors->get('prod_status') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Product Price</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputEstimatedBudget">Product Price Reguler</label>
                <input type="number" name="prod_price_reguler" class="form-control" autocomplete="off" value="{{ old('prod_price_reguler') }}">
                @if ($errors->has('prod_price_reguler'))
                    @foreach ($errors->get('prod_price_reguler') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Product Price Discount</label>
                <input type="number" name="prod_discount" class="form-control" autocomplete="off" value="{{ old('prod_discount') }}" >
              </div>
              {{-- <div class="form-group">
                <label for="inputEstimatedDuration">Product Price Fixed</label>
                <input type="number" name="prod_after_discount" class="form-control" disabled autocomplete="off" value="{{ old('prod_after_discount') }}">
              </div> --}}
            </div>
            
            <!-- /.card-body -->
          </div>
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Photo</h3>
              
            </div>
            
            <div class="card-body p-0">
              <table class="table">
                <tbody>
                  <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="customFile" accept=".jpeg,.jpg,.png,.gif,.webp,.svg,.jfif">
                    <label class="custom-file-label" for="customFile" selected>Image</label>
                      @if ($errors->has('prod_image'))
                          @foreach ($errors->get('prod_image') as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      @endif
                    <script>
                      $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                      });
                    </script>
                  </div>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Create new Product" class="btn btn-success float-right">
        </div>
      </div>
</form>
@endsection