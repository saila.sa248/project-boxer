@extends('admin.layouts.master')
@section('content')
<div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <form method="POST" action="{{ route('prod-update') }}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value="{{$product->prod_id}}" name="prod_id">
              <h3 class="card-title">Product Edit</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputCode">Product Code</label>
                <input type="text" id="inputCode" class="form-control" name="prod_code" autocomplete="off" value="{{ $product->prod_name }}">
                @if ($errors->has('prod_code'))
                    @foreach ($errors->get('prod_code') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input type="text" id="inputName" class="form-control" value="{{ $product->prod_name }}" name="prod_name" autocomplete="off">
                @if ($errors->has('prod_name'))
                    @foreach ($errors->get('prod_name') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Description</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="prod_desc" value="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" placeholder="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" readonly></textarea>
              </div>
              <div class="form-group">
                <label for="inputStatus">Product Status</label>
                <select id="inputStatus" class="form-control custom-select" name="prod_status">
                  <option disabled>--- Select one ---</option>
                    <option value="1" {{ $product->prod_status == 1 ? 'selected' : '' }}>Active</option>
                    <option value="2" {{ $product->prod_status == 2 ? 'selected' : '' }}>Inactive</option>
                  </select>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Product Price</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputEstimatedBudget">Product Price Reguler</label>
                <input autocomplete="off" type="number" id="inputEstimatedBudget" class="form-control" value="{{$product->prod_price_reguler}}" step="1" name="prod_price_reguler">
                @if ($errors->has('prod_price_reguler'))
                    @foreach ($errors->get('prod_price_reguler') as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Product Price Discount</label>
                <input autocomplete="off" type="number" id="inputSpentBudget" class="form-control" value="{{$product->prod_discount}}" step="1" name="prod_discount">
              </div>
              {{-- <div class="form-group">
                <label for="inputEstimatedDuration">Product Price Fixed</label>
                <input autocomplete="off" type="number" id="inputEstimatedDuration" class="form-control" value="{{$product->prod_after_discount}}" step="0.1" readonly>
              </div> --}}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Photo</h3>
            </div>
            
            <div class="card-body p-0">
              <table class="table">
                <tbody>
                  <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="customFile" accept=".jpeg,.jpg,.png,.gif,.webp,.svg,.jfif">
                    <label class="custom-file-label" for="customFile" selected>Image</label>
                    <script>
                      $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                      });
                    </script>
                  </div>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="/product" class="btn btn-secondary">Back</a>
          <input type="submit" value="Save Changes" class="btn btn-success float-right">
        </div>
      </div>
  </form>
@endsection