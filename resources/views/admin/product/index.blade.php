@extends('admin.layouts.master')
@section('content')
<div class="card">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                {!! \Session::get('success') !!}
                {{Session::forget('success')}}
                {{Session::save('success')}}
            </div>
        @endif
        <div class="card-header">
          <h3 class="card-title"><a href="{{ route('product_add') }}" style="text-decoration:none"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;Product</h3>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 20%">
                          Product Code
                      </th>
                      <th style="width: 20%">
                          Product Image
                      </th>
                      <th style="width: 20%">
                          Product Name
                      </th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                      <th style="width: 30%" class="text-center">
                        Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @php $i = 1;
                  @endphp
                  @foreach($product as $prod)
                  <tr>
                      <td>
                        {{ $i++ }}
                      </td>
                      <td>
                        {{ $prod->prod_code }}
                      </td>
                      <td>
                          <img width="50%" src="{{ asset('/images/product/'.$prod->prod_image) }}">
                      </td>
                      <td>
                          <a>
                          {{$prod->prod_name}}
                          </a>
                      </td>
                      <td class="project-state">
                          @if($prod->prod_status == 1) 
                            <span class="badge badge-success">Active</span>
                          @else
                            <span class="badge badge-danger">Inactive</span>
                          @endif
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href="/product_detail/{{$prod->prod_id}}">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="{{ route('product_edit', $prod->prod_id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
@endsection