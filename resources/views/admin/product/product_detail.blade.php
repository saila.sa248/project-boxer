@extends('admin.layouts.master')
@section('content')
<!-- Default box -->
<div class="card card-solid">
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-sm-6">
              <div class="col-12">
                <img src="{{ asset('/images/product/'.$product->prod_image) }}" class="product-image" alt="Product Image">
              </div>
            </div>
            <div class="col-12 col-sm-6">
              <h2 class="my-3"><i>Product Code : {{$product->prod_code}}</i></h2>
              <h1 class="my-1">Product Name : {{$product->prod_name}}</h1>
              @if($product->prod_status == 1) 
                <h2 class="badge badge-success">Active</h2>
              @else
                <h2 class="badge badge-danger">Inactive</h2>
              @endif
              {{-- <h3 class="my-3">{{$product->prod_status}}</h3> --}}
              <textarea id="inputDescription" class="form-control" rows="4" name="prod_desc" value="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" placeholder="Celana kolor / Boxer motif nyaman di pakai (tidak panas) cocok untuk di pakai sehari2 bisa untuk cewe dan cowo (unisex)


Bahan katun stretch ( Halus, adem, melar nyaman saat di pakai )

Ukuran All Size

✔ Untuk ukuran celana 29' - 36' ✔ Panjang celana -/+ 40cm ✔ Lingkar pinggang 110cm ✔ Lingkar paha -/+ 70cm ✔ Ada kantong di sebelah kanan ✔ Jahitan Rapi sesuai standart garmen


Silahkan chat untuk motif lainnya, atau menanyakan ketersediaan stok motif, pesan satuan/banyak bisa pilih motif 😊
" readonly></textarea>

              <hr>

              <div class="bg-gray py-2 px-3 mt-4">
                <h4 class="mb-0">
                  <small><del class="text-danger">@currency($product->prod_price_reguler)</del></small>
                </h4>
                @php $disc = $product->prod_price_reguler - $product->prod_discount @endphp
                <h4 class="mt-0">
                  @currency($disc)
                </h4>
              </div>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            {{-- <a href="#" class="btn btn-secondary">Cancel</a> --}}
            <a href="/product" class="btn btn-secondary float-right">Back</a>
          </div>
        </div>

@endsection