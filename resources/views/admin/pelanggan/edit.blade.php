@extends('admin.layouts.master')
@section('content')
<div class="card card-primary card-outline">
    <div class="card-body box-profile">
        <div class="text-center mb-4">
            <img class="profile-user-img img-fluid img-circle" src="{{asset('images/fav-icon.png')}}" alt="User profile picture">
        </div>

        <h3 class="profile-username text-center">Profile Customer</h3><br><br>
        @foreach($cust as $c)
        <form method="POST" action="{{route('customer-update')}}">
        @csrf
        <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
            <b>Customer ID</b> <a class="float-right">{{$c->cust_id}}</a>
            <input type="hidden" value="{{$c->cust_id}}" name="cust_id">
            </li>
            <li class="list-group-item">
            <b>Nama</b> <a class="float-right">{{$c->cust_name}}</a>
            </li>
            <li class="list-group-item">
            <b>Email</b> <a class="float-right">{{$c->cust_email}}</a>
            </li>
            <li class="list-group-item">
            <b>No Telepon</b> <a class="float-right">{{$c->cust_no_telp}}</a>
            </li>
            <li class="list-group-item">
            <b>Alamat</b> <a class="float-right">{{$c->cust_alamat}}</a>
            </li>
            <li class="list-group-item">
            <b>Provinsi</b> <a class="float-right">{{$c->cust_provincy}}</a>
            </li>
            <li class="list-group-item">
            <b>Kota</b> <a class="float-right">{{$c->cust_kota}}</a>
            </li>
            <li class="list-group-item">
            <b>Kecamatan</b> <a class="float-right">{{$c->cust_kecamatan}}</a>
            </li>
            <li class="list-group-item">
            <b>Kode Pos</b> <a class="float-right">{{$c->cust_kode_pos}}</a>
            </li>
            <li class="list-group-item">
            <b>Status</b> 
            @if($c->cust_status == 2)
            <a class="float-right">Inactive</a>
            @else
            <a class="float-right">Active</a>
            @endif
        </ul>
        @if($c->cust_status == 1)
        <button type="submit" class="btn btn-block bg-gradient-danger" name="cust_status" value="2">Non-aktifkan Customer</button>
        @elseif($c->cust_status == 2)
        <button type="submit" class="btn btn-block bg-gradient-success" name="cust_status" value="1">Aktifkan Customer</button>
        @endif
        <a href="/cust" class="btn btn-block bg-gradient-secondary">Back</a>
        </form>
        @endforeach
    </div>
    <!-- /.card-body -->
</div>
@endsection