@extends('admin.layouts.master')
@section('content')
<div class="card">
        @if (\Session::has('success-aktif'))
            <div class="alert alert-success">
                {!! \Session::get('success-aktif') !!}
                {{Session::forget('success-aktif')}}
                {{Session::save('success-aktif')}}
            </div>
        @elseif(Session::has('success-inactive'))
            <div class="alert alert-danger">
                {!! \Session::get('success-inactive') !!}
                {{Session::forget('success-inactive')}}
                {{Session::save('success-inactive')}}
            </div>
        @endif
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 30%">
                          Full Name
                      </th>
                      <th style="width: 15%">
                          Email
                      </th>
                      <th style="width: 8%" class="text-center">
                        Status
                      </th>
                      <th style="width: 30%" class="text-center">
                        Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @php
                   $i = 1;   
                  @endphp
                  @foreach($cust as $c)
                  <tr>
                      <td>{{$i++}}</td>
                      <td>{{$c->cust_name}}</td>
                      <td>{{$c->cust_email}}</td>
                      @if($c->cust_status == 2)
                        <td style="width: 30%" class="text-center"><span class="badge badge-danger">Inactive</span></td>
                      @else
                        <td style="width: 30%" class="text-center"><span class="badge badge-success">Active</span></td>
                      @endif
                     <td class="project-actions text-center">
                        <a class="btn btn-primary btn-sm" href="{{ route('customer-detail', $c->cust_id) }}">
                            <i class="fas fa-folder">
                            </i>
                            View
                        </a>
                        @if($c->cust_status == 1)
                        <a class="btn btn-danger btn-sm" href="{{ route('customer-edit', $c->cust_id) }}">
                          <i class="fas fa-">
                          </i>
                          Nonaktifkan Customer
                        </a>
                        @else
                        <a class="btn btn-info btn-sm" href="{{ route('customer-edit', $c->cust_id) }}">
                          <i class="fas fa-pencil-alt">
                          </i>
                          Activasi Customer
                        </a>
                        @endif
                     </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
@endsection