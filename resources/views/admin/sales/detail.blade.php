@extends('admin.layouts.master')
@section('content')
<div class="card card-primary card-outline">
    <div class="card-body box-profile">
        <h3 class="profile-username text-center">Detail Sales</h3><br><br>
        @foreach($checkout as $co)
        <form method="POST" action="{{route('sales-update')}}">
        @csrf
        <input type="hidden" value="{{$co->co_id}}" name="co_id">
        <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
            <b>Nama Customer</b> <a class="float-right">{{$co->cust_name}}</a>
            </li>
            <li class="list-group-item">
            <b>No Telepon</b> <a class="float-right">{{$co->cust_no_telp}}</a>
            </li>
            <li class="list-group-item">
            <b>Alamat</b> <a class="float-right">{{$co->cust_alamat}}</a>
            </li>
            <li class="list-group-item">
            <b>Provinsi - Kota - Kecamatan - Kode Pos</b> <a class="float-right">{{$co->cust_provincy}} - {{$co->cust_kota}} - {{$co->cust_kecamatan}} - {{$co->cust_kode_pos}}</a>
            </li>
            <li class="list-group-item">
            <b>Status</b> 
            @if($co->co_status == 1) 
                <a class="float-right">Open</a>
            @elseif ($co->co_status == 2)
                <a class="float-right">Confirmed</a>
            @elseif ($co->co_status == 3)
                <a class="float-right">Process</a>
            @endif
            <li class="list-group-item">
            <div class="card card-primary collapsed-card">
                <div class="card-header">
                  <h3 class="card-title">Bukti Bayar</h3>
    
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                      <i class="fas fa-plus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body" style="display: none;">
                    <img class="center-bukti-bayar" src="{{ asset('/images/bayar/'.$co->bukti_bayar) }}">
                  </div>
                  <div class="form-group">
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
            <style>
            .center-bukti-bayar {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 35%;
                max-width: 35%;
                }
            </style>
        </ul>
        <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table m-0">
                <thead>
                <tr>
                    <th style="text-align: left">No</th>
                    <th>Nama Product</th>
                    <th style="text-align: right">Sales Product</th>
                    <th style="text-align: right">Discount Product</th>
                    <th style="text-align: right">Quantity Product</th>
                    <th style="text-align: right">Total Sales Product</th>
                </tr>
                </thead>
                <tbody>
                @php $i = 1;
                $total_co = 0;
                $sum = 0;
                @endphp
                @foreach($checkout_item as $coi)
                @php
                $product =  $coi::join('product', function($query) {
                                $query->on('product.prod_id', 'checkout_item.prod_id');
                            })->where('product.prod_id', $coi->prod_id)->where('coi_id', $coi->coi_id)->first();
                @endphp
                <tr>
                <td align="left">{{ $i++ }}</td>
                <td>{{ $product->prod_name }}</a></td>
                <td align="right">Rp {{ number_format($coi->coi_prod_price_reguler,0,'','.') }}</td>
                <td align="right">Rp {{ number_format($coi->coi_prod_discount,0,'','.') }}</td>
                <td align="right">{{ $coi->coi_prod_qty }}</td>
                <td align="right">Rp {{ number_format($coi->coi_fix_sales,0,'','.') }}</td>
                <input type="hidden" value="{{$sum += $coi->coi_fix_sales}}">
                </tr>
                @endforeach
            </tbody>
        </table>
        <li class="list-group-item"><b>Total :</b><a class="float-right">Rp {{number_format($sum,0,'','.')}}</a>
        </div>
            @if($co->co_status == 1) 
            <button type="submit" class="btn btn-block bg-gradient-success" name="co_status" value="2">Confirm</button>
            @elseif ($co->co_status == 2)
            <button type="submit" class="btn btn-block btn-warning" name="co_status" value="3">Proses</button>
            @endif
            <a href="/cust" class="btn btn-block bg-gradient-secondary">Back</a>
        </form>
        @endforeach
    </div>
    <!-- /.card-body -->
</div>
@endsection