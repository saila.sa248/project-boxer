@extends('admin.layouts.master')
@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
        {{Session::forget('success')}}
        {{Session::save('success')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ $status1 }}</h3>
                <p>Open</p>
            </div>
            <div class="icon">
                <i class="fas fa-shopping-cart"></i>
            </div>
            {{-- <a href="#" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
            </a> --}}
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ $status2 }}</h3>
                <p>Confirmed</p>
            </div>
            <div class="icon">
                <i class="fa fa-check-circle"></i>
            </div>
            {{-- <a href="#" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
            </a> --}}
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{ $status3 }}</h3>
                <p>Process</p>
            </div>
            <div class="icon">
                <i class="fa fa-hourglass-start"></i>
            </div>
            {{-- <a href="#" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
            </a> --}}
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ $status4 }}</h3>
                <p>Total Order</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-bag"></i>
            </div>
            {{-- <a href="#" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
            </a> --}}
        </div>
    </div>
                <!-- TABLE: LATEST ORDERS -->
                <div class="col-lg-12 col-sm-6">
                <div class="card">
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                        <th style="text-align: center width: 2%">No</th>
                        <th style="width: 20%">Order ID</th>
                        <th style="width: 30%">Nama Customer</th>
                        <th style="text-align: center" width="10%">Tanggal</th>
                        <th style="width: 8%" class="text-left">Status</th>
                        <th style="text-align: right">Sales</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1;
                    $total_co = 0;
                    @endphp
                    @foreach($checkout as $co)
                    <tr>
                    <td align="center" width="2%">{{ $i++ }}</td>
                    <td width="20%"><a href="{{route('sales-detail', $co->co_id)}}">{{ $co->co_code }}</a></td>
                    <td width="30%">{{ ucwords($co->cust_name) }}</td>
                    <td align="left" width="15%">{{date('d F Y', strtotime($co->created_at))}}</td>
                    @if($co->co_status == 1) 
                        <td align="left"><span class="badge badge-info">Open</span></td>
                    @elseif ($co->co_status == 2)
                        <td align="left"><span class="badge badge-success">Confirmed</span></td>
                    @elseif ($co->co_status == 3)
                        <td align="left"><span class="badge badge-warning">Process</span></td>
                    @endif
                      <td>
                        @foreach($co->checkoutItem as $coi) 
                            @php
                            $total_co = $coi->where('co_id', $co->co_id)->sum('coi_fix_sales');
                            @endphp
                        @endforeach
                        <div class="sparkbar" data-color="#00a65a" data-height="20" align="right">Rp {{ number_format($total_co, 0, '', '.') }}</div>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
</div>
@endsection