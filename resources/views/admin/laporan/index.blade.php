@extends('admin.layouts.master')
@section('content')
<div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th style="text-align:center">No</th>
                      <th>Tanggal</th>
                      <th style="text-align:right">Sales</th>
                      <th style="text-align:right">Discount</th>
                      <th style="text-align:right">Total Sales</th>
                    </tr>
                    </thead>
                    @php $i = 1;
                    @endphp
                    @foreach($laporan as $laporan)
                    <tbody>
                    <tr>
                      <td style="text-align:center">{{$i++}}</td>
                      <td>{{date('d F Y', strtotime($laporan->day))}}</td>
                      {{-- @foreach($sales as $sal) --}}
                      <td style="text-align:right">Rp {{ number_format($laporan->prod_price_reguler, 0, '', '.') }}</td>
                      {{-- @endforeach --}}
                      {{-- @foreach($discount as $dis) --}}
                      <td style="text-align:right">Rp {{ number_format($laporan->discount,0, '', '.') }}</td>
                      {{-- @endforeach --}}
                      {{-- @foreach($laporan as $lap) --}}
                      <td style="text-align:right">Rp {{ number_format($laporan->total,0,'','.') }}</td>
                      {{-- @endforeach --}}
                    </tr>
                    </tbody>
                    @endforeach
                  </table>
                </div>
                <!-- /.col -->
              </div>
@endsection